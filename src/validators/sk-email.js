import { SkFormValidator } from "../sk-form-validator.js";


export class SkEmail extends SkFormValidator {

    get msg() {
        return 'The value must me a valid email';
    }

    get attrName() {
        return 'sk-email';
    }

    validate(field) {
        let value = field.getAttribute('value');
        let re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        if (! re.test(String(value).toLowerCase())) {
            return this.msg;
        }
    }
}