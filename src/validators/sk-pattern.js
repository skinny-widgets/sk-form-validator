import { SkFormValidator } from "../sk-form-validator.js";


export class SkPattern extends SkFormValidator {

    get msg() {
        return 'The value must match requirements';
    }

    get attrName() {
        return 'sk-pattern';
    }

    validate(field) {
        let value = field.getAttribute('value');
        if (value !== null) {
            let pattern = new RegExp(field.getAttribute('sk-pattern'));
            if (!pattern.test((value).toString())) {
                return this.msg;
            }
        }
    }
}