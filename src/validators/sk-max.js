import { SkFormValidator } from "../sk-form-validator.js";


export class SkMax extends SkFormValidator {

    get msg() {
        return 'The value is less than required';
    }

    get attrName() {
        return 'sk-max';
    }

    validate(field) {
        let value = field.getAttribute('value');
        let fieldType = field.getAttribute('type');
        let min  = field.getAttribute('sk-min');
        if (fieldType === 'number') {
            if (parseFloat(value) < parseFloat(min)) {
                return this.msg;
            }
        } else {
            if (value === null || value.length < min) {
                return this.msg;
            }
        }
    }
}