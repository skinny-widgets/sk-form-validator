import { SkFormValidator } from "../sk-form-validator.js";


export class SkRequired extends SkFormValidator {

    get msg() {
        return 'The value must match requirements';
    }

    get attrName() {
        return 'sk-required';
    }

    validate(field) {
        if (field.tagName === 'SK-CHECKBOX' || field.tagName === 'SK-SWITCH' || field.type === 'CHECKBOX') {
            if (! field.hasAttribute('checked')) {
                return this.msg;
            }
        } else {
            let value = field.getAttribute('value');
            if (value === null || value === undefined || value === '') {
                return this.msg;
            }
        }
    }
}